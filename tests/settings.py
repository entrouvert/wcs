TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'en-us'
ALLOWED_HOSTS = ['*']
PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher']
DEBUG_PROPAGATE_EXCEPTIONS = True
AFTERJOB_MODE = 'tests'
