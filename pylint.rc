[MASTER]
persistent=yes
ignore=vendor,Bouncers,ezt.py

[MESSAGES CONTROL]
disable=
    abstract-method,
    arguments-differ,
    attribute-defined-outside-init,
    broad-except,
    broad-exception-raised,
    consider-using-dict-comprehension,
    consider-using-f-string,
    consider-using-set-comprehension,
    cyclic-import,
    duplicate-code,
    fixme,
    global-variable-undefined,
    import-outside-toplevel,
    inconsistent-return-statements,
    invalid-name,
    keyword-arg-before-vararg,
    missing-class-docstring,
    missing-function-docstring,
    missing-module-docstring,
    no-member,
    non-parent-init-called,
    not-callable,
    possibly-unused-variable,
    possibly-used-before-assignment,
    protected-access,
    raise-missing-from,
    redefined-argument-from-local,
    redefined-builtin,
    redefined-outer-name,
    signature-differs,
    stop-iteration-return,
    super-init-not-called,
    superfluous-parens,
    too-many-branches,
    too-many-instance-attributes,
    too-many-lines,
    too-many-locals,
    too-many-nested-blocks,
    too-many-return-statements,
    too-many-statements,
    undefined-loop-variable,
    unnecessary-comprehension,
    unnecessary-lambda-assignment,
    unspecified-encoding,
    unsubscriptable-object,
    unsupported-membership-test,
    unused-argument,
    use-implicit-booleaness-not-comparison


[TESTOPTIONS]
ignored-parents=wcs.qommon.TenantAwareThread


[REPORTS]
output-format=parseable


[BASIC]
no-docstring-rgx=__.*__|_.*
class-rgx=[A-Z_][a-zA-Z0-9_]+$
function-rgx=[a-zA_][a-zA-Z0-9_]{2,70}$
method-rgx=[a-z_][a-zA-Z0-9_]{2,70}$
const-rgx=(([A-Z_][A-Z0-9_]*)|([a-z_][a-z0-9_]*)|(__.*__)|register|urlpatterns)$
good-names=_,i,j,k,e,x,Run,,setUp,tearDown,r,p,s,v,fd

[TYPECHECK]

# Tells whether missing members accessed in mixin class should be ignored. A
# mixin class is detected if its name ends with "mixin" (case insensitive).
ignore-mixin-members=yes

# List of classes names for which member attributes should not be checked
# (useful for classes with attributes dynamically set).
ignored-classes=SQLObject,WSGIRequest,Publisher,NullSessionManager

# List of members which are set dynamically and missed by pylint inference
# system, and so shouldn't trigger E0201 when accessed.
generated-members=objects,DoesNotExist,id,pk,_meta,base_fields,context

# List of method names used to declare (i.e. assign) instance attributes
defining-attr-methods=__init__,__new__,setUp


[VARIABLES]
init-import=no
dummy-variables-rgx=_|dummy|i
additional-builtins=_,N_,ngettext
good-names=_,i,j,k,e,x,Run,,setUp,tearDown,r,p,s,v,fd

[SIMILARITIES]
min-similarity-lines=6
ignore-comments=yes
ignore-docstrings=yes


[MISCELLANEOUS]
notes=FIXME,XXX,TODO


[FORMAT]
max-line-length=160
max-module-lines=2000
indent-string='    '


[DESIGN]
max-args=10
max-locals=15
max-returns=6
max-branches=12
max-statements=50
max-parents=7
max-attributes=7
min-public-methods=0
max-public-methods=50
max-positional-arguments=10
