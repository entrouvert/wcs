# w.c.s. - web application for online forms
# Copyright (C) 2005-2023  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import json

import requests

from wcs.formdef_base import get_formdefs_of_all_kinds
from wcs.mail_templates import MailTemplate
from wcs.qommon import _
from wcs.workflows import Workflow

from . import TenantCommand


class Command(TenantCommand):
    support_all_tenants = True

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            '-f',
            '--filename',
            metavar='FILENAME',
            help='replacements file (JSON with "conditions" and "templates" dictionaries)',
        )
        parser.add_argument(
            '-u',
            '--url',
            metavar='URL',
            help='url to replacements file (JSON with "conditions" and "templates" dictionaries)',
        )

    def handle(self, *args, **options):
        if options.get('url'):
            self.replacements = requests.get(options.get('url'), timeout=15).json()
        else:
            with open(options.get('filename')) as fp:
                self.replacements = json.load(fp)
        for domain in self.get_domains(**options):
            self.init_tenant_publisher(domain, register_tld_names=False)
            self.replace()

    def replace(self):
        change_message = _('Automatic migration from Python to Django conditions and templates')
        for formdef in get_formdefs_of_all_kinds(order_by='id', ignore_migration=True):
            changed = False
            for field in formdef.fields or []:
                changed |= self.replace_condition(field)
                changed |= self.replace_prefill(field)
                if field.key == 'page':
                    changed |= self.replace_post_conditions(field)
            if changed:
                formdef.store(comment=change_message)
        for workflow in Workflow.select(ignore_migration=True):
            changed = False
            for item in workflow.get_all_items():
                changed |= self.replace_condition(item)
                changed |= self.replace_workflow_expressions(item)
            for action in workflow.global_actions or []:
                for trigger in action.triggers or []:
                    if getattr(trigger, 'anchor', None) == 'python':
                        old_value = f'={trigger.anchor_expression}'
                        value = self.replace_expression(old_value)
                        if value != old_value:
                            trigger.anchor = 'template'
                            trigger.anchor_expression = None
                            trigger.anchor_template = value
                            changed = True
            if changed:
                workflow.store(comment=change_message)
        for mail_template in MailTemplate.select(ignore_migration=True):
            changed = self.replace_expression_attr(mail_template, 'subject')
            if getattr(mail_template, 'attachments', None):
                new_value = [
                    self.replace_expression(x, ignore_equal_marker=True) for x in mail_template.attachments
                ]
                if new_value != mail_template.attachments:
                    mail_template.attachments = new_value
                    changed = True
            if changed:
                mail_template.store(comment=change_message)

    def replace_condition(self, obj):
        condition = getattr(obj, 'condition', None)
        if not condition or condition.get('type') == 'django':
            return False
        if condition['value'] in self.replacements['conditions']:
            obj.condition['type'] = 'django'
            obj.condition['value'] = self.replacements['conditions'][condition['value']]
            return True
        return False

    def replace_post_conditions(self, obj):
        changed = False
        for post_condition in getattr(obj, 'post_conditions', None) or []:
            condition = post_condition.get('condition')
            if not condition or condition.get('type') == 'django':
                continue
            if condition['value'] in self.replacements['conditions']:
                condition['type'] = 'django'
                condition['value'] = self.replacements['conditions'][condition['value']]
                changed = True
        return changed

    def replace_prefill(self, field):
        prefill = getattr(field, 'prefill', None)
        if not prefill or prefill.get('type') != 'formula' or not prefill.get('value'):
            return False
        if prefill['value'] in self.replacements['templates']:
            field.prefill['type'] = 'string'
            field.prefill['value'] = self.replacements['templates'][prefill['value']]
            return True
        return False

    def replace_workflow_expressions(self, action):
        changed = False
        if action.key in ('set-backoffice-fields', 'update_user_profile'):
            for field in action.fields or []:
                old_value = field.get('value')
                field['value'] = self.replace_expression(old_value)
                changed |= bool(field['value'] != old_value)
        if action.key in ('create_carddata', 'create_formdata', 'edit_carddata'):
            for mapping in action.mappings or []:
                changed |= self.replace_expression_attr(mapping, 'expression')
        if action.key in ('sendmail', 'sendsms') and action.to:
            for i, to in enumerate(action.to[:]):
                new_value = self.replace_expression(to)
                if new_value != to:
                    changed = True
                    action.to[i] = new_value
        if action.key in ('sendmail', 'register-comment') and getattr(action, 'attachments', None):
            new_value = [self.replace_expression(x, ignore_equal_marker=True) for x in action.attachments]
            if new_value != action.attachments:
                action.attachments = new_value
                changed = True
        if action.key == 'webservice_call':
            if action.qs_data:
                for key, value in list(action.qs_data.items()):
                    new_value = self.replace_expression(value)
                    if new_value != value:
                        action.qs_data[key] = new_value
                        changed = True
            if action.post_data:
                for key, value in list(action.post_data.items()):
                    new_value = self.replace_expression(value)
                    if new_value != value:
                        action.post_data[key] = new_value
                        changed = True

        for attribute in (
            # create_carddata
            'user_association_template',
            # dispatch
            'role_id',
            'variable',
            # export to model
            'filename',
            # external workflow
            'target_id',
            # geolocate
            'address_string',
            'map_variable',
            'photo_variable',
            # jump
            'timeout',
            # notification
            'title',
            'body',
            'target_url',
            'users_template',
            # redirect/wscall
            'url',
            # sendmail/sms
            'subject',
            'body',
        ):
            changed |= self.replace_expression_attr(action, attribute)
        return changed

    def replace_expression_attr(self, obj, attribute):
        if not hasattr(obj, attribute):
            return False
        value = getattr(obj, attribute)
        new_value = self.replace_expression(value)
        if new_value != value:
            setattr(obj, attribute, new_value)
            return True
        return False

    def replace_expression(self, value, ignore_equal_marker=False):
        if not isinstance(value, str) or (not value.startswith('=') and not ignore_equal_marker):
            return value
        python_expression = value.removeprefix('=')
        if python_expression in self.replacements['templates']:
            return self.replacements['templates'][python_expression]
        return value
