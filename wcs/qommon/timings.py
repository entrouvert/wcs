# w.c.s. - web application for online forms
# Copyright (C) 2005-2024  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import time


class TimingsMixin:
    timings = None

    def start_timing(self, name):
        if not self.timings:
            self.timings = []
        self.timings.append({'name': name, 'start': time.time()})
        return self.timings[-1]

    def stop_timing(self, timing):
        timing['end'] = time.time()
        timing['duration'] = timing['end'] - timing['start']
        return timing['duration']

    def add_timing_mark(self, name, relative_start=None):
        if not self.timings:
            return
        timestamp = time.time()
        duration = timestamp - (
            relative_start or self.timings[-1].get('timestamp') or self.timings[-1].get('start')
        )
        self.timings.append({'mark': name, 'timestamp': timestamp, 'duration': duration})
