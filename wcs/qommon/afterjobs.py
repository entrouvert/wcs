# w.c.s. - web application for online forms
# Copyright (C) 2005-2010  Entr'ouvert
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses/>.

import datetime
import json
import sys
import time
import traceback
import uuid

from django.utils.encoding import force_str
from django.utils.timezone import localtime
from quixote import get_publisher, get_request, get_response
from quixote.directory import Directory

import wcs.sql

from . import N_, _, errors
from .storage import StorableObject
from .timings import TimingsMixin


class AfterJobStatusDirectory(Directory):
    def _q_lookup(self, component):
        get_request().ignore_session = True
        get_response().set_content_type('application/json')
        try:
            job = AfterJob.get(component)
        except KeyError:
            raise errors.TraversalError()

        if job.status == 'failed':
            message = job.failure_label or _('failed')
        else:
            message = _(job.status)
            completion_status = job.get_completion_status()
            if completion_status:
                message = f'{message} {completion_status}'
        return json.dumps(
            {
                'status': job.status,
                'message': str(message),
            }
        )


class AfterJob(wcs.sql.SqlAfterJob, TimingsMixin):
    _names = 'afterjobs'
    _reset_class = False

    label = None
    status = None
    creation_time = None
    completion_time = None
    current_count = None
    total_count = None
    failure_label = None

    _last_store_time = 0

    execute = None

    def __init__(self, label=None, cmd=None, **kwargs):
        super().__init__(id=str(uuid.uuid4()))
        if label:
            self.label = force_str(label)
        self.done_action_label_arg = kwargs.pop('done_action_label', None)
        self.done_action_url_arg = kwargs.pop('done_action_url', None)
        self.done_button_attributes_arg = kwargs.pop('done_button_attributes', None)
        self.creation_time = localtime()
        self.job_cmd = cmd
        self.status = N_('registered')
        self.kwargs = kwargs

    def __repr__(self):
        return '<AfterJob id:%s cmd:%r>' % (self.id, self.job_cmd)

    @classmethod
    def get(cls, id, *args, **kwargs):
        try:
            uuid.UUID(id)
        except ValueError:
            raise KeyError(id)
        return super().get(id, *args, **kwargs)

    def mark_as_failed(self, message=None):
        self.status = 'failed'
        if message:
            self.failure_label = str(message)
            self.store()

    def done_action_label(self):
        return self.done_action_label_arg

    def done_action_url(self):
        return self.done_action_url_arg % {'job_id': self.id}

    def done_action_attributes(self):
        return self.done_button_attributes_arg

    def increment_count(self, amount=1):
        self.current_count = (self.current_count or 0) + amount
        # delay storage to avoid repeated writes on slow storage
        if self.id and time.time() - self._last_store_time > 1:
            self.store_count()

    def get_completion_status(self):
        current_count = self.current_count or 0

        if not current_count:
            return ''

        if not self.total_count:
            return _('%(current_count)s (unknown total)') % {'current_count': current_count}

        return _('%(current_count)s/%(total_count)s (%(percent)s%%)') % {
            'current_count': int(current_count),
            'total_count': self.total_count,
            'percent': int(current_count * 100 / self.total_count),
        }

    def run(self, *, publisher=None, spool=False):
        if self.completion_time:
            return

        if publisher is None:
            publisher = get_publisher()

        if spool and self.id and self.execute:
            from django.conf import settings

            if 'uwsgi' in sys.modules and settings.WCS_MANAGE_COMMAND:
                from .spooler import run_after_job

                self.store()
                run_after_job.spool(tenant_dir=publisher.app_dir, job_id=self.id)
                return

        self.status = N_('running')
        self.store_status()
        try:
            if self.execute:
                self.execute()
            else:
                self.job_cmd(job=self)
        except Exception as e:
            if getattr(self, 'raise_exception', False):
                raise
            if self.status == 'running':
                # capture/record error unless it's been set already
                publisher.capture_exception(sys.exc_info())
                publisher.record_error(exception=e, record=False, notify=True)
                self.exception = traceback.format_exc()
                self.store()
                self.status = N_('failed')
        else:
            if self.status == 'running':
                self.status = N_('completed')
        self.completion_time = localtime()
        self.store_status()

    def store(self, *args, **kwargs):
        if self.id:
            self._last_store_time = time.time()
            return super().store(*args, **kwargs)

    def __getstate__(self):
        if getattr(self, 'done_action_label_arg', None):
            self.done_action_label_arg = force_str(self.done_action_label_arg)
        obj_dict = self.__dict__.copy()
        if '_last_store_time' in obj_dict:
            del obj_dict['_last_store_time']
        if not isinstance(self.job_cmd, str):
            obj_dict['job_cmd'] = None
        return obj_dict

    @classmethod
    def clean(cls):
        from wcs.sql_criterias import And, Equal, Less, Or

        now = localtime()
        cls.wipe(
            clause=[
                Or(
                    [
                        And(
                            [
                                Equal('status', 'completed'),
                                Less('completion_time', now - datetime.timedelta(hours=1)),
                            ]
                        ),
                        Less('creation_time', now - datetime.timedelta(days=2)),
                    ]
                )
            ]
        )

    def get_api_status_url(self):
        return get_request().build_absolute_uri('/api/jobs/%s/' % self.id)

    def get_processing_url(self):
        return '/backoffice/processing?job=%s' % self.id


class FileAfterJob(StorableObject):
    # legacy class for migration
    _names = 'afterjobs'
    _reset_class = False
